import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGalleryComponent } from './components/admin-gallery/admin-gallery.component';

const routes: Routes = [
  {
    path: '',
    component: AdminGalleryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
