import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AboutComponent } from './components/about/about.component';
import { AdminModule } from './admin/admin.module';
import { AuthGuard } from './guards/auth.guard';
import { OrderProductsComponent } from './components/order-products/order-products.component';
import { MladenCustomFieldsModule } from './mladen-custom-fields/mladen-custom-fields.module';
import { TimerComponent } from './components/timer/timer.component';

const routes : Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'gallery',
        component: GalleryComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'order-products',
        component: OrderProductsComponent
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'mladen-custom-fields',
        loadChildren: './mladen-custom-fields/mladen-custom-fields.module#MladenCustomFieldsModule'
    },
    {
        path: 'profiles-guest',
        loadChildren: './profiles-guest/profiles-guest.module#ProfilesGuestModule'
    },
    {
        path: 'profiles',
        loadChildren: './profiles/profiles.module#ProfilesModule'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'timer',
        component: TimerComponent
    },
    {
        path: 'hotel',
        loadChildren: './hotel/hotel.module#HotelModule'
    },
    {
        path: 'rooms',
        loadChildren: './rooms/rooms.module#RoomsModule'
    },
    {
        path: 'housekeeping',
        loadChildren: './housekeeping/housekeeping.module#HousekeepingModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})


export class AppRouting {

}