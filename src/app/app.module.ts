import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SliderComponent } from './components/slider/slider.component';
import { AppRouting } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeadSectionComponent } from './components/head-section/head-section.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatDialogModule, MatLabel, MatInputModule, MatFormFieldModule, MatIconModule, MatSelectModule, MatTableModule, MatSortModule, MatSelectionList, MatListModule, MatPaginatorModule} from '@angular/material';
import { FormComponent } from './components/form/form.component';
import { InputOutputComponent } from './components/input-output/input-output.component';
import { ButtonComponent } from './components/button/button.component';
import { OrderProductsComponent } from './components/order-products/order-products.component';

import { SimpleTimer } from 'ng2-simple-timer';

import * as moment from 'moment';
import { TimerComponent } from './components/timer/timer.component';
import { AuthInterceptor } from './shared/Interceptor';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    HomeComponent,
    GalleryComponent,
    AboutComponent,
    ContactComponent,
    HeadSectionComponent,
    FormComponent,
    InputOutputComponent,
    ButtonComponent,
    OrderProductsComponent,
    TimerComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatListModule,
    FormsModule,
    MatPaginatorModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent, HeadSectionComponent],
  entryComponents: [FormComponent]
})
export class AppModule { 
}
