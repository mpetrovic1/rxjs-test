import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  private hotels;

  constructor(
    private authService : AuthService,
    private router : Router
  ) { }

  ngOnInit() {
    this.authService.getHotels().subscribe(data => {
      this.hotels = data;
    });
  }

  public onSelect(selectedHotel){
    let selected = selectedHotel['_value'][0];

    let choosenHotel = this.hotels.filter(hotel => hotel.Id === selected)[0];
    
    this.authService.modify(choosenHotel.AccountId, choosenHotel.HotelName, choosenHotel.Id).subscribe(data => {
      localStorage.setItem('token', data['token']);
      this.router.navigate(['hotel']);
    });
  }
}
