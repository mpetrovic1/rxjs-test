import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private authService : AuthService,
    private router : Router
  ) { }

  public loginForm : FormGroup = this.fb.group({
    username : ['', Validators.required],
    password : ['', Validators.required]
  });
  

  ngOnInit() {

  }

  public login(){
    this.authService.login(this.loginForm.get('password').value, this.loginForm.get('username').value)
    .subscribe(data => {
      if(data.hasOwnProperty('token')){
        localStorage.setItem('token', data['token']);
        this.router.navigate(['/auth/hotels']);
      }
    });
  }

}
