import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  // animations: [
  //   trigger('showColumnsList', [
  //     state('true', style({
  //       // transition?
  //       display: 'block'
  //     })),
  //     state('false', style({
  //       display: 'none'
  //     })),
  //     transition('* => *', [
  //       animate('0.4s')
  //     ])
  //   ]),
    
  // ]
})
export class AboutComponent implements OnInit {

  public niz = [1,2,3];  
  public HOURHAND;
  public MINUTEHAND;
  public SECONDHAND;
  

  constructor() { }

  ngOnInit() {

    var interval = setInterval(this.runTheClock, 1000);
  }

  public runTheClock() {

    

    this.HOURHAND = document.querySelector("#hour");
    this.MINUTEHAND = document.querySelector("#minute");
    this.SECONDHAND = document.querySelector("#second");

    var date = new Date();
    console.log(date);
    let hr = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    console.log("Hour: " + hr + " Minute: " + min + " Second: " + sec);
    
    let hrPosition = (hr*360/12)+(min*(360/60)/12);
    let minPosition = (min*360/60)+(sec*(360/60)/60);
    let secPosition = sec*360/60;
  
    hrPosition = hrPosition+(3/360);
    minPosition = minPosition+(6/60);
    secPosition = secPosition+6;

    // this.HOURHAND = {
    //   transform: "rotate(" + hrPosition + "deg)"
    // }
    // this.MINUTEHAND = {
    //   transform: "rotate(" + minPosition + "deg)"
    // }
    // this.SECONDHAND = {
    //   transform: "rotate(" + secPosition + "deg)"
    // }

    // this.HOURHAND = hrPosition;
    // this.MINUTEHAND = minPosition;
    // this.SECONDHAND = secPosition;
    
    this.HOURHAND.style.transform = "rotate(" + hrPosition + "deg)";
    this.MINUTEHAND.style.transform = "rotate(" + minPosition + "deg)";
    this.SECONDHAND.style.transform = "rotate(" + secPosition + "deg)";
  }
}
