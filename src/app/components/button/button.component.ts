import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() btnName : string;
  @Output() message = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    
  }


  hello(){
    this.message.emit("Hello from button!");
  }

}
