import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { FormComponent } from '../form/form.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styles: []
})
export class ContactComponent implements OnInit {

  public btnName = "Contact";


  public orderForm : FormGroup;
//  public items : FormArray;
  public items : any[];

  constructor(
    private fb : FormBuilder,
    private dialog : MatDialog
  ) { }

  // public form : FormGroup = new FormGroup({
  //   name: new FormControl('', [Validators.minLength(5), Validators.maxLength(20)]),
  //   email: new FormControl('', Validators.email),
  //   message: new FormControl('', Validators.minLength(10))
  // });


  // vrati posle


  public form : FormGroup;



  public onSubmit(){
    console.log(this.form.value);
  }

  public openModal(){
    this.dialog.open(FormComponent);
  }
  
  ngOnInit() {

    this.form = this.fb.group({
      name: ['', [Validators.minLength(5), Validators.maxLength(20)]],
      email: ['', Validators.email],
      message: ['', Validators.minLength(10)],
      //items : this.fb.array([ this.createItem() ])
      items : new FormArray([])
    })

    this.items = [
      {
        id: 1,
        name: "Item 1"
      },
      {
        id: 2,
        name: "Item 2"
      },
      {
        id: 3,
        name: "Item 3"
      }
    ]

    for(let i=0; i<this.items.length; i++){
      (this.form.get("items") as FormArray).push(
        new FormControl(i === 0)
      );
    }

    console.log(this.form);
  }

  public changeOption(){
    this.form.value['items'] = this.form.value['items'].filter(x=>x==true);
    console.log(this.form.value);
  }

  public getMessage(msg : string){
    alert(msg);
  }

}
