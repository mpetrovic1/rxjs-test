import { Component, OnInit } from '@angular/core';
import { FloorsService } from 'src/app/services/floors.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styles : []
})
export class GalleryComponent implements OnInit {

  constructor(private floorsService : FloorsService) { }

  ngOnInit() {
    this.getFloors();
  }

  getFloors() {
    this.floorsService.getAll().subscribe(floors => {
      console.log(floors);
    });
  }

}
