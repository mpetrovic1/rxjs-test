import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { OrderProductsService } from 'src/app/services/order-products.service';
import { OrderProduct } from 'src/app/interfaces/order-product';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import {Subscription} from 'rxjs';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';


@Component({
  selector: 'app-order-products',
  templateUrl: './order-products.component.html',
  styles: [],
  animations: [
    trigger('openClose', [
      state('open', style({
        // ?
        // display: 'block',
        opacity: 1
      })),
      state('close', style({
        // display: 'none',
        opacity: 0
      })),
      transition('* => *', [
        animate('0.35s')
      ])
      
      
    ])
  ]

})
export class OrderProductsComponent implements OnInit {
  
  // static true?
  @ViewChild(MatSort, {static: true}) sort : MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;

  public formOrderProducts: FormGroup;
  public orderProducts = new MatTableDataSource<OrderProduct>();
  public allOrderProducts = [];
  private subscription = new Subscription();

  public pageSizeOptions = [1, 3, 5, 10, 20, 50, 100];
  public defaultPage = 5;

  public isListOfColumnsOpen : string = 'close';
//  public orderProducts : OrderProduct[];
  //public static counter : number = 1;

//  columnsToDisplay: string[] = ['position'];

  public columns = [
    {
      name: 'productName',
      displayName: 'Name of product',
      isDisplayed: true
    },
    {
      name: 'order1',
      displayName: 'Order 1',
      isDisplayed: false
    },
    {
      name: 'order2',
      displayName: 'Order 2',
      isDisplayed: true
    }
  ]

  //['productName', 'order2'];
  displayedColumns: string[] = this.columns.filter((x) => x.isDisplayed).map(function(x) {return x.name});

  constructor(
    private service : OrderProductsService,
    private fb : FormBuilder
  ) { }

  ngOnInit() {
    this.getAll();
    this.formOrderProducts = this.fb.group({items:this.fb.array([])});
  }

  public onSearch(value : string){
    //this.orderProducts.filter(x => Object.keys(x).some(s => s[x].includes(value)));
    //this.orderProducts.data.filter(x => Object.keys(x).some(s => x[s].include(value)));

    //this.orderProducts.data = this.allOrderProducts.filter(x => Object.values.filter(z => 1==1));

    //this.orderProducts.data = this.allOrderProducts.filter((data) =>  JSON.stringify(data).toLowerCase().split(',').filter(p => p.split(',').filter(z => z.split(':'))[1].indexOf(value.toLowerCase()) !== -1));
    //this.orderProducts.data = this.allOrderProducts.filter((data) =>  JSON.stringify(data).toLowerCase().indexOf(value.toLowerCase()) !== -1);
    // blizu?
//    this.orderProducts.data = this.allOrderProducts.filter((data) => JSON.stringify(data).split(',').filter(x => x.split(':'))[1].indexOf(value.toLowerCase()) != -1);

    let nesto = '';
    this.orderProducts.data = this.allOrderProducts.filter(x => Object.values(x).some(z => z.toString().toLowerCase().indexOf(value.toLowerCase()) != -1));

    // let pera = [
    //   {'ime': 'pera', 'prezime':'peric', 'godine':28},
    //   {'ime': 'mika', 'prezime':'mikic', 'godine':30}
    // ]
    // let array1 = pera.filter(item => Object.values(item).some(x => x == 'mikic'));
    // console.log(array1);
    // var str = "ime: pera, prezime: mika, nesto: zika";
    // let x = str.split(',').filter(x => x.split(':')[1].toLowerCase().indexOf(value.toLowerCase()) != -1);

//    return 1;
  }

  public getAll(){
    this.subscription.add(this.service.getAll().subscribe(
      response => {
        this.orderProducts.data = response as OrderProduct[];
        this.allOrderProducts = response as OrderProduct[];

        this.orderProducts.sort = this.sort;
        this.orderProducts.paginator = this.paginator;

//        let form = this.fb.array([]);

let form = this.formOrderProducts.get('items') as FormArray;
    this.allOrderProducts.forEach(
      x => {
        form.push(this.fb.group({
          order1: [x.order1, Validators.required],
          order2: [x.order2, Validators.required],
          name: [x.productName],
          id: [x.id]
        }));
      }
    )

    console.log(this.formOrderProducts.value);
        
      }
    ))
  }

  getIndex(row){
    const index = this.orderProducts.data.indexOf(row);
    console.log(row);
    return index;
  }



  public toggleListOfColumns(){
//    this.isListOfColumnsOpen == 'close' ? 'open' : 'close';
    if(this.isListOfColumnsOpen == 'open'){
      this.isListOfColumnsOpen = 'close';
    }
    else {
      this.isListOfColumnsOpen = 'open';
    }
    console.log(this.isListOfColumnsOpen)
  }

  public addRemoveColumn(selected : any){
    //this.displayedColumns = selected.map(x => x.value);
    // let tmp = [];
    // for(let column of this.columns){
    //   for(let matColumn of selected){
    //     if(column.name === matColumn.value)
    //       tmp.push(column.name);
    //   }
    // }

    let selectedColumns = selected.map(x => x.value);
    let foundColumns = this.columns.filter(x => selectedColumns.indexOf(x.name) != -1);

    this.displayedColumns = foundColumns.map(x => x.name);
    
  }

  onSave(){
    console.log(this.formOrderProducts.value);
  }
  

}
