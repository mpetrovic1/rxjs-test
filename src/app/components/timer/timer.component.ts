import { Component, OnInit } from '@angular/core';
import * as moment from 'moment-timezone';
import { Clock } from 'src/app/interfaces/clock';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

  hours: string;
  minutes: string;
  seconds: string;
  private timerId = null;

  private jun = moment;

  public grinich : Clock = {} as Clock;
  public local : Clock = {} as Clock;
  public hotel : Clock = {} as Clock;
  public clocks = [this.grinich, this.local, this.hotel];

  constructor(
    
  ){

  }

  ngOnInit() {
    console.log(this.jun.tz.names());

    this.updateTime();
    this.timerId = this.updateTime();
  }

  ngOnDestroy() {
    clearInterval(this.timerId);
  }

  private setLocalTime() {
    const time = new Date(Date.now());

    this.local.name = 'Your Local Time';
    this.local.hours = this.leftPadZero(time.getHours());
    this.local.minutes = this.leftPadZero(time.getMinutes());
    this.local.seconds = this.leftPadZero(time.getSeconds());
  }

  private setHotelTime() {
    let hours = this.jun.tz('Asia/Bangkok').format('h');
    let minutes = this.jun.tz('Asia/Bangkok').format('m');
    let seconds = this.jun.tz('Asia/Bangkok').format('s');

    this.hotel.name = 'Asia/Bangkok';
    this.hotel.hours = this.leftPadZero(hours);
    this.hotel.minutes = this.leftPadZero(minutes);
    this.hotel.seconds = this.leftPadZero(seconds);
  }

  private setGrinichTime(){
    let hours = this.jun.tz('UTC').format('h');
    let minutes = this.jun.tz('UTC').format('m');
    let seconds = this.jun.tz('UTC').format('s');

    this.grinich.name = 'UTC Time';
    this.grinich.hours = this.leftPadZero(hours);
    this.grinich.minutes = this.leftPadZero(minutes);
    this.grinich.seconds = this.leftPadZero(seconds);
  }

  private updateTime() {
    setInterval(() => {
      this.setLocalTime();
      this.setHotelTime();
      this.setGrinichTime();
    }, 1000);
  }

  private leftPadZero(value: number) {
    return value < 10 ? `0${value}` : value.toString();
  }

}
