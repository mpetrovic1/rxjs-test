export const config = "https://pms-api.k8s-ci.frontdeskanywhere.net/";

export const apis = {
    floors : "assets/data/floors.json",
    login: "api/tokenMaster/auth",
    hotels: "api/userMaster/hotels",
    modify: "api/token/modify",
    rooms: "api/rooms",
    roomTypes: "api/room-types",
    roomClasses: "api/room-classes",
    floorsApi: "api/floors",
    housekeepingDashboard: "api/housekeepingdashboard",
    employees: "api/employee",
    housekeepingStatuses: "api/housekeeping-statuses"
};