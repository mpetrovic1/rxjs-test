import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from "angular-jwt";
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public name : string;

  constructor(
    private authService : AuthService
  ) { }

  ngOnInit() {
    this.name = this.authService.getInformation().given_name;

  }

}
