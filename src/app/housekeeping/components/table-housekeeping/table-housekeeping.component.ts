import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Subscription, Observable, forkJoin } from 'rxjs';
import { HousekeepingService } from 'src/app/services/housekeeping.service';
import { Housekeeping } from 'src/app/interfaces/housekeeping';
import { HousekeepingFilter } from 'src/app/interfaces/housekeeping-filter';
import { FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Employee } from 'src/app/interfaces/employee';
import * as cloneDeep from 'lodash/cloneDeep';


@Component({
  selector: 'app-table-housekeeping',
  templateUrl: './table-housekeeping.component.html',
  styleUrls: ['./table-housekeeping.component.css'],
  animations: [
    trigger('showColumnsList', [
      state('true', style({
        // transition?
        display: 'block'
      })),
      state('false', style({
        display: 'none'
      })),
      transition('* => *', [
        animate('0.4s')
      ])
    ])
  ]
})
export class TableHousekeepingComponent implements OnInit {
  
  housekeeperCtrl = new FormControl();
  filteredHousekeepers: Observable<string[]>;
  housekeepers: string[] = [];
  allHousekeepers: string[] = [];
  employees : string[];
  selectedHousekeepers = [];

  statusCtrl = new FormControl();
  filteredStatuses: Observable<string[]>;
  statuses: string[] = [];
  allStatuses: string[] = [];
  statusesFromApi : string[];
  selectedStatuses = [];

  @ViewChild('housekeeperInput', {static: false}) housekeepersInput: ElementRef<HTMLInputElement>;
  @ViewChild('housekeeperAuto', {static: false}) matHousekeeperAutocomplete: MatAutocomplete;

  @ViewChild('statusInput', {static: false}) statusesInput: ElementRef<HTMLInputElement>;
  @ViewChild('statusAuto', {static: false}) matStatusAutocomplete: MatAutocomplete;
  

  public add(chip: string, event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add housekeeper only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    switch(chip) {
      case 'housekeeper': {
        if(!this.matHousekeeperAutocomplete.isOpen) {
      
          // // Add our housekeeper
          if ((value || '').trim()) {
             this.housekeepers.push(value.trim());
          }
          // // Reset the input value
          if (input) {
            input.value = '';
          }
    
          this.housekeeperCtrl.setValue(null);
      }
    }
    break;
      case 'status': {
        if(!this.matStatusAutocomplete.isOpen) {

          if ((value || '').trim()) {
             this.statuses.push(value.trim());
          }

          if (input) {
            input.value = '';
          }
    
          this.statusCtrl.setValue(null);
      }
    }
    break;

    }
  }

  public remove(chip : string,housekeeper: string): void {
    switch(chip) {
      case 'housekeeper': {
        const index = this.housekeepers.indexOf(housekeeper);
        
        this.housekeepers.splice(index, 1);
        this.selectedHousekeepers.splice(index, 1);
        this.filterByHousekeeper();

      }
      break;
      case 'status': {
        
        const index = this.statuses.indexOf(housekeeper);
        // if (index >= 0) {
          this.statuses.splice(index, 1);
        //}
        this.selectedStatuses.splice(index, 1);
   
        this.filterByHousekeeper();
      }
      break;
    }
    

     
  }

  selected(chip: string, event: MatAutocompleteSelectedEvent): void {
    switch(chip){
      case 'housekeeper': {
        this.housekeepers.push(event.option.viewValue);
        this.housekeepersInput.nativeElement.value = '';
        this.housekeeperCtrl.setValue(null);
        
        this.selectedHousekeepers.push(event.option.viewValue);
    
        this.filterByHousekeeper()
      }
      break;
      case 'status': {
        this.statuses.push(event.option.viewValue);
        this.statusesInput.nativeElement.value = '';
        this.statusCtrl.setValue(null);
        
        this.selectedStatuses.push(event.option.viewValue);
    
        this.filterByHousekeeper();
      }
      break;
      
    }
    
  }

  private _filter(chip: string, value: string): string[] {
    const filterValue = value.toLowerCase();
    switch(chip){
      case 'housekeeper': {
        return this.allHousekeepers.filter(housekeeper => housekeeper.toLowerCase().indexOf(filterValue) === 0);
      }
      break;
      case 'status': {
        return this.allStatuses.filter(status => status.toLowerCase().indexOf(filterValue) === 0);
      }
      break;
      
    }
    
  }


  // ------------------------------------------------------------------------------ //

  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort;

  private isVisibleColumnsList : boolean = false;
  public pageSizeOptions = [2, 5, 10, 20, 50, 100];
  public defaultPageSize = 10;

  private subscription = new Subscription();

  public matDataSource = new MatTableDataSource<Housekeeping>();
  // kao pomocni niz
  public dataSource = [];

  public checkboxesForDelete = [];

  public viewByFilter: string = 'housekeeper';
  public matTables: string[] = [];
  public matTablesAll: string[] = [];
  public dataSources: MatTableDataSource<any>[] = [];
  public dataSourcesAll: MatTableDataSource<any>[] = [];
  

  public unique = 0;

  public filters: HousekeepingFilter[] = [
    {
        name: 'housekeeper',
        displayName: 'Housekeeper'
    },
    {
        name: 'housekeeping_status_id',
        displayName: 'Housekeeping Status'
    },
    {
        name: 'floor',
        displayName: 'Floor'
    },
    {
        name: 'reservation_status',
        displayName: 'Room Status'
    },
    {
        name: 'reservation_status_tonight',
        displayName: 'Room Status Tonight'
    },
    {
        name: 'roomType',
        displayName: 'Room Type'
    },
    {
        name: 'roomClass',
        displayName: 'Room Class'
    },
    {
        name: 'priority',
        displayName: 'Priority'
    }
  ];


  public columns = [
    {
      name: "chb",
      displayName: "Check",
      display: true
    },
    {
      name: "room_id",
      displayName: "Room ID",
      display: true
    },
    {
      name: "housekeeping_status_id",
      displayName: "House Keeping Status ID",
      display: true
    },
    {
      name: "reservation_status",
      displayName: "Reservation Status",
      display: true
    },
    {
      name: "reservation_status_tonight",
      displayName: "Reservation Status Tonight",
      display: true
    },
    {
      name: "housekeeper",
      displayName: "Housekeeper",
      display: true
    },
    {
      name: "priority",
      displayName: "Priority",
      display: true
    },
    {
      name: "work_orders",
      displayName: "Work Orders",
      display: true
    }
  ]

  public displayedColumns : string[] = this.columns.filter(column => column.display === true).map(column => column.name);

  constructor(
    private service : HousekeepingService
  ) { 
    this.filteredHousekeepers = this.housekeeperCtrl.valueChanges.pipe(
      startWith(null),
      map((housekeeper: string | null) => housekeeper ? this._filter('housekeeper',housekeeper) : this.allHousekeepers.slice()));
    
    this.filteredStatuses = this.statusCtrl.valueChanges.pipe(
      startWith(null),
      map((status: string | null) => status ? this._filter('status',status) : this.allStatuses.slice()));
        
  }

  ngOnInit() {
    // this.service.getEmployees().subscribe(data => {
    //   this.allHousekeepers = (data['data'] as Employee[]).map(item => item['firstName'] + ' ' + item['lastName']);
    //   //console.log(this.allHousekeepers);
    // })

    // this.service.getStatuses().subscribe(data => {
    //   this.allStatuses = (data['data']).map(item => item['name']);
    //   //console.log(this.allHousekeepers);
    // })
    

    this.getAll();
  }

  public getColumnsForDisplay(){
    return this.displayedColumns;
  }

  public changeVisibilityOfColumns(){
    this.isVisibleColumnsList = !this.isVisibleColumnsList;
  }

  public onSearch(value : string){
    this.matDataSource.filter = value.trim().toLowerCase();
  }

  public _async_load: Observable<any>;
  private _subscription = new Subscription();
  _allStatuses;
  _allHousekeepers;
  _dataSource;

  public getAll(){

    this._allStatuses = this.service.getStatuses();
    this._allHousekeepers = this.service.getEmployees();
    this._dataSource = this.service.getAll();

    const _forSubscribe: any[] = [this._allStatuses, this._allHousekeepers, this._dataSource];
    
    this._async_load = forkJoin(_forSubscribe);
    this._subscription.add(this._async_load.subscribe(
      data => {
        this.allStatuses = (data[0]['data']).map(item => item['name']);
        this.allHousekeepers = (data[1]['data'] as Employee[]).map(item => item['firstName'] + ' ' + item['lastName']);;

        this.dataSource = data[2]['data'] as Housekeeping[];
        this.matDataSource.data = data[2]['data'] as Housekeeping[];

       this.dataSource.forEach(x => x.housekeeping_status_id = data[0]['data'].filter(item => item.id == x.housekeeping_status_id).map(y => y.name)[0]);

        let viewByFilterTmp = this.viewByFilter;
        let x = this.dataSource.map((item) => {
          return item[viewByFilterTmp]
        });

        this.matTables = x.filter(this.onlyUnique);
        this.matTablesAll = x.filter(this.onlyUnique);

        this.matDataSource.paginator = this.paginator;
        this.matDataSource.sort = this.sort;
        
        this.matTables.forEach(x => {
          this.getMatDataSource(x);
        });

        console.log(this.dataSources);

      }
    ));


    // this.subscription.add(this.service.getAll().subscribe(
    //   response => {
    //     console.log(this.allStatuses);
    //     // probati kasnije bez kastovanja

    //     this.dataSource = response['data'] as Housekeeping[];
    //     this.matDataSource.data = response['data'] as Housekeeping[];

    //     //this.dataSource.map(item =>)['housekeeping'].filter(this.onlyUnique);
    //     //console.log(this.onlyUnique);

    //     let viewByFilterTmp = this.viewByFilter;
    //     let x = this.dataSource.map((item) => {
    //       return item[viewByFilterTmp]
    //     });

    //     this.matTables = x.filter(this.onlyUnique);
    //     this.matTablesAll = x.filter(this.onlyUnique);

    //     this.matDataSource.paginator = this.paginator;
    //     this.matDataSource.sort = this.sort;

    //     //console.log(this.matDataSource.data);

    //   }
    // ))

//    setTimeout(()=>console.log(this.matTables), 5500);

  }

  public addRemoveColumn(selected : any){
    let selectedColumns = selected.map(x => x.value);
    let foundColumns = this.columns.filter(x => selectedColumns.indexOf(x.name) != -1);

    this.displayedColumns = foundColumns.map(x => x.name); 
  }

  public onCheck(item){
    if(item.checked)
      this.checkboxesForDelete.push(<number>item.source.value);
    else
    this.checkboxesForDelete = this.checkboxesForDelete.filter(x => x != <number>item.source.value);

    //console.log(this.checkboxesForDelete);
  }

  filterData(event, filter: HousekeepingFilter) {
    
    this.viewByFilter = filter.name;

    let viewByFilterTmp = this.viewByFilter;
    let x = this.dataSource.map((item) => {
      return item[viewByFilterTmp]
    });

    this.matTables = x.filter(this.onlyUnique);
    this.matTablesAll = x.filter(this.onlyUnique);
    this.dataSources = [];
    this.dataSourcesAll = [];
    this.matTables.forEach(x => {
      this.getMatDataSource(x);
    })
    this.filterByHousekeeper();

//    this.dataSources = [];
    
  }


  onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
  }



// usage example:
// var a = ['a', 1, 'a', 2, '1'];
// var unique = a.filter( onlyUnique ); // returns ['a', 1, 2, '1']

  public getMatDataSource(table){
    let dataSource = JSON.parse(JSON.stringify(this.matDataSource.data));
    
    let column = this.viewByFilter;
    dataSource = this.matDataSource.data.filter((item) => {
    
    let viewByFilterTmp = this.viewByFilter;
      return item[viewByFilterTmp] == table
    });

    this.dataSources.push(new MatTableDataSource(dataSource));
    this.dataSourcesAll.push(new MatTableDataSource(dataSource));
    
    //return dataSource;
  }



  public filterByHousekeeper(){
    
    let dataSource = JSON.parse(JSON.stringify(this.dataSource));
    let dataSourceForPush = [];    

    this.dataSources.forEach((x, index) => {
      x.data = cloneDeep(this.dataSourcesAll[index].data);
    })

    if(this.selectedHousekeepers.length > 0 || this.selectedStatuses.length > 0) {
      if(this.selectedHousekeepers.length > 0 && this.selectedStatuses.length > 0) {
        dataSource.forEach((x) => {
          
          if(this.selectedHousekeepers.includes(x['housekeeper']) && this.selectedStatuses.includes(x['housekeeping_status_id'])) {
            // dataSourceForPush.push(x);
            // this.dataSources.forEach(dataSource => {
            //   dataSource.data = dataSource.data.filter(y => {
            //     return y['housekeeper'] == x['housekeeper'] && y['housekeeping_status_id'] == x['housekeeping_status_id'];
            //   });
            // })
            
            this.dataSources.forEach((x, index) => {
              x.data = x.data.filter(item => {
               return this.selectedHousekeepers.includes(item['housekeeper']) && this.selectedStatuses.includes(item['housekeeping_status_id']);
             });
           });

          }  
        })
      }
      else if(this.selectedHousekeepers.length > 0) {
        console.log(this.selectedHousekeepers);
        //this.dataSources = cloneDeep(this.dataSourcesAll);
        
        
        this.dataSources.forEach((x, index) => {
           x.data = x.data.filter(item => {
            return this.selectedHousekeepers.includes(item['housekeeper']);
          });
        });

//        this.dataSources[0].data = [];

        // dataSource.forEach((x) => {
        //   if(this.selectedHousekeepers.includes(x['housekeeper'])) {
        //     dataSourceForPush.push(x);
            
        //     this.dataSources.forEach(dataSource => {
        //       dataSource.data = dataSource.data.filter(y => {
        //         return y['housekeeper'] == x['housekeeper'];
        //       });
        //     })

        //   }  
        // })
      }
      else {
        dataSource.forEach((x) => {
          if(this.selectedStatuses.includes(x['housekeeping_status_id'])) {
            dataSourceForPush.push(x);
            this.dataSources.forEach(dataSource => {
              dataSource.data = dataSource.data.filter(y => {
                return y['housekeeping_status_id'] == x['housekeeping_status_id'];
              });
            })
          }  
        })
      }
        
    }




    // if(this.selectedHousekeepers.length > 0 || this.selectedStatuses.length > 0) {
    //   dataSource.forEach((x) => {

    //     if(this.selectedHousekeepers.length > 0 && this.selectedStatuses.length > 0)
    //       if(this.selectedHousekeepers.includes(x['housekeeper']) && this.selectedStatuses.includes(x['housekeeping_status_id'])) {
    //         console.log('first')
    //         dataSourceForPush.push(x);
    //       }
        
    //     else if(this.selectedHousekeepers.length > 0)
    //       if(this.selectedHousekeepers.includes(x['housekeeper'])) {
    //         console.log('second')
    //         dataSourceForPush.push(x);
    //       }

    //     else
    //       if(this.selectedStatuses.includes(x['housekeeping_status_id'])) {
    //         console.log('third');
    //         dataSourceForPush.push(x);         
    //       }
    //   });
    // }
    else {
//      dataSourceForPush = dataSource;
      
    }

  //   //dataSource.some(r=> this.selectedHousekeepers.indexOf(r.housekeeper) >= 0);

  //  //this.matDataSource.data = dataSource.filter(x => x.housekeeper == housekeeper);

  //   if(this.viewByFilter === 'housekeeper') {
  //     if(this.selectedHousekeepers.length > 0) {

  //       // setTimeout(()=>{
  //       //   console.log(this.selectedHousekeepers);
  //       //   console.log(this.selectedStatuses);
  //       // }, 3000);


  //       if(this.selectedHousekeepers.length > 0)
  //         this.matTables = JSON.parse(JSON.stringify(this.matTablesAll.filter(x => this.selectedHousekeepers.includes(x))));
  //       // if(this.selectedStatuses.length > 0)
  //       //   this.matTables = JSON.parse(JSON.stringify(this.matTablesAll.filter((x) => {
  //       //     return this.selectedHousekeepers.includes(x)
  //       //   })));
          
  //     }
  //     else
  //       this.matTables = JSON.parse(JSON.stringify(this.matTablesAll));
  //   }
  //   else {
  //     if(this.selectedHousekeepers.length > 0) {

  //       // this.matTables = JSON.parse(JSON.stringify(this.matTablesAll.forEach((x) => {
  //       //   let index = this.matTables.indexOf(x);
  //       //   if(!this.matDataSource.data.some(x => x[this.viewByFilter] === x))
  //       //     this.matTables.splice(index, 1);
  //       // })));

  //       // floor 1, floor 2, floor 3
  //       let matTables = JSON.parse(JSON.stringify(this.matTablesAll));
  //       this.matTables = matTables.filter((tableName) => {
  //         return this.matDataSource.data.some(x => x[this.viewByFilter] == tableName);
  //       });
  //     }

  //     else
  //       this.matTables = JSON.parse(JSON.stringify(this.matTablesAll));
  //   }

  //   if(this.selectedHousekeepers.length > 0)
  //     this.matDataSource.data = dataSource.filter((x)=>{
  //       return this.selectedHousekeepers.includes(x.housekeeper);
  //   });
    
  //   else
  //     this.matDataSource.data = dataSource;
    }




  public filterByStatus(){
    
  //   let dataSource = JSON.parse(JSON.stringify(this.dataSource));
    
  //   //dataSource.some(r=> this.selectedHousekeepers.indexOf(r.housekeeper) >= 0);

  //  //this.matDataSource.data = dataSource.filter(x => x.housekeeper == housekeeper);

  //   if(this.viewByFilter === 'housekeeper') {
  //     if(this.selectedHousekeepers.length > 0) {

  //       this.matTables = JSON.parse(JSON.stringify(this.matTablesAll.filter(x => this.selectedHousekeepers.includes(x))));
  //     }
  //     else
  //       this.matTables = JSON.parse(JSON.stringify(this.matTablesAll));
  //   }

  //   if(this.selectedHousekeepers.length > 0)
  //     this.matDataSource.data = dataSource.filter((x)=>{
  //       return this.selectedHousekeepers.includes(x.housekeeper);
  //   });
    
  //   else
  //     this.matDataSource.data = dataSource;
  }


  public sorting(event, table){
    let direction = event.direction;
    console.log(event)
//    console.log(table)

    this.matTables = [];

    let dataSource = JSON.parse(JSON.stringify(this.dataSources[table].data));
    let dataSourceTmp = [];

    if(event.direction === 'asc') {
      this.dataSources[table].data = this.dataSources[table].data.sort((a,b) => a[event.active] > b[event.active] ? 1 : -1);      
      console.log('asc')
    }
    else {
      this.dataSources[table].data = this.dataSources[table].data.sort((a,b) => a[event.active] < b[event.active] ? 1 : -1);
      console.log('desc')
    }

    // let x = "asc" > "asd";
    // console.log(x);

    // this.matDataSource.data = this.matDataSource.data.filter(x => {
    //   return dataSource.filter(y => {
    //     return JSON.stringify(y) != JSON.stringify(x)
    //   });
    // });

    // dataSource.forEach(x => {
    //   this.matDataSource.data.push(x);
    // })
    

    //this.dataSources.push(new MatTableDataSource(this.dataSources[table].data));
    //console.log(this.dataSources[table].data);
    // dataSource.forEach(x => {
    //   let value = x[event.active];
    //   dataSourceTmp.forEach(y => {
    //     let value2 = x[event.active];

    //     if(value > value2)
    //     {

    //     }

    //   });
    // });
    
  }
  public compare(x, y) {
    // if(direction === 'asc') {
      let a = x['housekeeper'];
      let b = y['housekeeper'];
      if(a > b)
        return 1;
      else if(a < b)
        return -1;
      else
        return 0;
    // }
    // else {
    //   if(a > b)
    //     return 1;
    //   else if(a < b)
    //     return -1;
    //   else
    //     return 0;
    //}
  }

}