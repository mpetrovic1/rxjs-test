import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableHousekeepingComponent } from './components/table-housekeeping/table-housekeeping.component';

const routes: Routes = [
  {
    path: '',
    component: TableHousekeepingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HousekeepingRoutingModule { }
