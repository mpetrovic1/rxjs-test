import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HousekeepingRoutingModule } from './housekeeping-routing.module';
import { TableHousekeepingComponent } from './components/table-housekeeping/table-housekeeping.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TableHousekeepingComponent],
  imports: [
    CommonModule,
    HousekeepingRoutingModule,
    SharedModule
  ]
})
export class HousekeepingModule { }
