export interface Clock {
    name : string,
    hours : string,
    minutes : string,
    seconds : string
}
