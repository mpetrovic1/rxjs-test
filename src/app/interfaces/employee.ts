export interface Employee {
    id: number,
    code: string,
    firstName: string,
    lastName: string,
    phone: string,
    email: string
}
