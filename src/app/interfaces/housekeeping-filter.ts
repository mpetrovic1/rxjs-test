export interface HousekeepingFilter {
    name: string;
    displayName: string;
}
