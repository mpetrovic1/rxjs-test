export interface Housekeeping {
    floor: string;
    folio_no: number;
    housekeeper: string;
    housekeeping_status_id: number;
    priority: string;
    reservation_status: string;
    reservation_status_tonight: string;
    roomClass: string;
    roomType: string;
    room_id: number;
    title: string;
    work_orders: number;
}
