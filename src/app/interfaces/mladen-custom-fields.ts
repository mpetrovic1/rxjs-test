export interface MladenCustomFields {
    id : number,
    label : string,
    fieldName : string,
    order : number,
    isStandardFields : boolean,
    isEditFields : boolean,
    required : boolean,
    type : string,
    statusForGuest : true,
    statusForReservation : true,
    options : any[]
}
