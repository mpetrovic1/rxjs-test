export interface OrderProduct {
    id : number;
    productName : string;
    order1: number;
    order2: number;
}
