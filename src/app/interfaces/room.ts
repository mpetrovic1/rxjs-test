export interface Room {
    id: number;
    title: string;
    roomStatus: number;
    roomType: string;
    floor: string;
    roomClass: string;
}
