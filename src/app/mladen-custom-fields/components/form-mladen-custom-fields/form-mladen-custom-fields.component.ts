import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { MladenCustomFields } from 'src/app/interfaces/mladen-custom-fields';

@Component({
  selector: 'app-form-mladen-custom-fields',
  templateUrl: './form-mladen-custom-fields.component.html',
  styleUrls: ['./form-mladen-custom-fields.component.css']
})
export class FormMladenCustomFieldsComponent implements OnInit {

  public isAddOptionVisible = false;
  
  private id;
  private dataSource;

  private originalObj;
  private editedObj : MladenCustomFields = {} as MladenCustomFields;

  public customFieldTypes = [
    {
			value: 'TEXT',
			displayValue: 'Text'
    },
    {
			value: 'SELECT',
			displayValue: 'Select'
    },
    {
			value: 'MULTISELECT',
			displayValue: 'MultiSelect'
    },
    {
			value: 'COUNTRY',
			displayValue: 'Country'
    },
    {
			value: 'TEXTAREA',
			displayValue: 'TextArea'
    },
    {
			value: 'DATE',
			displayValue: 'Date'
    }
  ];

    // tbFieldName
  // ddlFieldType
  // nmbOrder
  private formMladenCustomFields : FormGroup;

  constructor(
    private dialogRef : MatDialogRef<FormMladenCustomFieldsComponent>,
    private fb : FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {

    this.id = this.data['id'];
    this.dataSource = this.data['dataSource'];
    
    if(this.id == null) {
      this.formMladenCustomFields = this.fb.group({
        'tbFieldName': ['', Validators.required],
        'ddlFieldType': ['', Validators.required],
        'nmbOrder': [0, Validators.required],
        'chbRequired': [false],
        'chbStatusForGuest': [false],
        'chbStatusForReservation' : [false],
        'options' : this.fb.array([])
      });
    }
    else {

      let obj = this.dataSource.filter(item => item.id === this.id)[0];
      // console.log(obj);
      this.originalObj = obj;

      this.formMladenCustomFields = this.fb.group({
        'id' : [obj.id],
        'tbFieldName': [obj.label, Validators.required],
        'ddlFieldType': [obj.type, Validators.required],
        'nmbOrder': [obj.order, Validators.required],
        'chbRequired': [obj.required],
        'chbStatusForGuest': [obj.statusForGuest],
        'chbStatusForReservation' : [obj.statusForReservation],
        'options' : this.fb.array([])
      });
      if(obj.options.length > 0){
        this.isAddOptionVisible = true;
        for(let option of obj.options){
          (<FormArray>this.formMladenCustomFields.get('options')).push(
            this.addOption(option.optionValue, option.priority, option.guestCustomFieldId, option.id)
          );
        }
      }
    }


  }

  public save(){

      let obj = this.formMladenCustomFields.value;
      this.makeEditedObj(obj);
    
      // EDIT
      if(this.id !== null) {
        for(let i of Object.keys(this.originalObj)){
        
          // zato sto je [] === [] false
          if(typeof(this.originalObj[i]) === 'object'){
            if(JSON.stringify(this.originalObj[i]) === JSON.stringify(this.editedObj[i])){
              delete this.editedObj[i];
            }
          }
          else {
            if(this.originalObj[i] === this.editedObj[i]){
              delete this.editedObj[i];
            }  
          }
  
        }
      }

      // izmenjene vrednosti sto se salje
      console.log(this.editedObj);
  }


  // funkcija koja je bila neophodna zbog nekonzistentnosti izmedju naziva polja u html-u i kljuceva koje vraca api
  private makeEditedObj(obj){
    
    this.editedObj.label = obj.tbFieldName;
    this.editedObj.type = obj.ddlFieldType;
    this.editedObj.order = obj.nmbOrder;
    this.editedObj.statusForGuest = obj.chbStatusForGuest;
    this.editedObj.statusForReservation = obj.chbStatusForReservation;
    this.editedObj.required = obj.chbRequired;
    this.editedObj.options = [];

    this.editedObj.id = obj.id;

    if(obj.options.length > 0){
      for(let i of obj.options){
        let option = {};
        option['id'] = i.id;
        option['guestCustomFieldId'] = i.guestCustomFieldId;
        option['optionValue'] = i.tbOptionValue;
        option['priority'] = i.nmbPriority;
        
        this.editedObj.options.push(option);
        
      }
    }
  }

  public addOption(tbOptionValue = '', nmbPriority = '', guestCustomFieldId = '', id = null) : FormGroup{
    
    return this.fb.group({
      'id' : [id],
      'guestCustomFieldId' : [guestCustomFieldId],
      'tbOptionValue' : [tbOptionValue, Validators.required],
      'nmbPriority' : [nmbPriority, Validators.required]
    })
  }

  public addOptionButtonClick() : void {
    // Mora cast zbog metoda push, jer ga nema u nadklasi AbstractControl (nadklasa za form array, control, group)
    for(let i=0; i<2; i++)
      (<FormArray>this.formMladenCustomFields.get('options')).push(this.addOption('','',this.id));
  }

  public removeOption(index){
      (<FormArray>this.formMladenCustomFields.get('options')).removeAt(index);
  }

  public isSaveDisabled(){
    return this.formMladenCustomFields.invalid;
  }

  public isAddVisible(value : string){
    let array = ["MULTISELECT", "SELECT", "RADIO"];
    array.includes(value) === true ? this.isAddOptionVisible = true : this.isAddOptionVisible = false;
  }

  public close(){
    this.dialogRef.close();
  }

}
