import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MladenCustomFieldsService } from 'src/app/services/mladen-custom-fields.service';
import { Subscription } from 'rxjs';
import { MladenCustomFields } from 'src/app/interfaces/mladen-custom-fields';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormMladenCustomFieldsComponent } from '../form-mladen-custom-fields/form-mladen-custom-fields.component';

@Component({
  selector: 'app-table-mladen-custom-fields',
  templateUrl: './table-mladen-custom-fields.component.html',
  styleUrls: ['./table-mladen-custom-fields.component.css'],
  animations: [
    trigger('showColumnsList', [
      state('true', style({
        // transition?
        display: 'block'
      })),
      state('false', style({
        display: 'none'
      })),
      transition('* => *', [
        animate('0.4s')
      ])
    ]),
    
  ]
})
export class TableMladenCustomFieldsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort;

  private isVisibleColumnsList : boolean = false;
  public pageSizeOptions = [2, 5, 10, 20, 50, 100];
  public defaultPageSize = 5;

  private subscription = new Subscription();

  public checkboxes = ["required", "statusForGuest", "statusForReservation"];

  // za mat tabelu
  public matDataSource = new MatTableDataSource<MladenCustomFields>();
  // kao pomocni niz
  public dataSource = [];
  
  public columns = [
    {
      name: "id",
      displayName: "#",
      display: true
    },
    {
      name: "label",
      displayName: "Field Name",
      display: true
    },
    {
      name: "type",
      displayName: "Field Type",
      display: true
    },
    {
      name: "options",
      displayName: "Option Value",
      display: true
    },
    {
      name: "order",
      displayName: "Order",
      display: true
    },
    {
      name: "required",
      displayName: "Required",
      display: true
    },
    {
      name: "statusForGuest",
      displayName: "Status For Guests",
      display: true
    },
    {
      name: "statusForReservation",
      displayName: "Status For Reservation",
      display: true
    },
    {
      name: "edit",
      displayName: "Edit",
      display: true
    }
  ]

  public displayedColumns : string[] = this.columns.filter(column => column.display === true).map(column => column.name);

  constructor(
    private service : MladenCustomFieldsService,
    private dialog : MatDialog
  ) { }

  ngOnInit() {
    this.getAll();
  }

  public getColumnsForDisplay(){
    return this.displayedColumns;
  }

  public changeVisibilityOfColumns(){
    this.isVisibleColumnsList = !this.isVisibleColumnsList;
  }

  public onSearch(value : string){
    this.matDataSource.filter = value.trim().toLowerCase();
//    this.matDataSource.data = this.dataSource.filter(field => Object.values(field).some(x => x.toString().toLowerCase().indexOf(value.toLowerCase()) != -1));
    
  }

  public getAll(){
    this.subscription.add(this.service.getAll().subscribe(
      response => {
        // probati kasnije bez kastovanja
        this.dataSource = response as MladenCustomFields[];
        this.matDataSource.data = response as MladenCustomFields[];

        this.matDataSource.paginator = this.paginator;
        this.matDataSource.sort = this.sort;
      }
    ))
  }

  public isCheckbox(value : string){
    return this.checkboxes.includes(value);
  }

  public openDialog(id = null){
    //console.log(id);

    this.dialog.open(FormMladenCustomFieldsComponent, {
      height: '550px',
      width: 'auto',
      data: {
        id: id,
        dataSource: this.dataSource
      }
    });
  }

  public addRemoveColumn(selected : any){
    let selectedColumns = selected.map(x => x.value);
    let foundColumns = this.columns.filter(x => selectedColumns.indexOf(x.name) != -1);

    this.displayedColumns = foundColumns.map(x => x.name); 
  }

}
