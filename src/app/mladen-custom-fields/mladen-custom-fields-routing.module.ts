import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableMladenCustomFieldsComponent } from './components/table-mladen-custom-fields/table-mladen-custom-fields.component';

const routes: Routes = [
  {
    path: '',
    component: TableMladenCustomFieldsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MladenCustomFieldsRoutingModule { }
