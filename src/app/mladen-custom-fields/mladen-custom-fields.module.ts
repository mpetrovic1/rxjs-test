import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MladenCustomFieldsRoutingModule } from './mladen-custom-fields-routing.module';
import { TableMladenCustomFieldsComponent } from './components/table-mladen-custom-fields/table-mladen-custom-fields.component';
import { FormMladenCustomFieldsComponent } from './components/form-mladen-custom-fields/form-mladen-custom-fields.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatInputModule, MatFormFieldModule, MatIconModule, MatSelectModule, MatTableModule, MatSortModule, MatListModule, MatPaginatorModule, MatSlideToggle, MatSlideToggleModule, MatCheckboxModule } from '@angular/material';

@NgModule({
  declarations: [TableMladenCustomFieldsComponent, FormMladenCustomFieldsComponent],
  imports: [
    CommonModule,
    MladenCustomFieldsRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatListModule,
    FormsModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatCheckboxModule
  ],
  entryComponents: [FormMladenCustomFieldsComponent]
})
export class MladenCustomFieldsModule { }
