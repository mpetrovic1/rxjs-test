import { Component, OnInit } from '@angular/core';
import { MladenCustomFieldsService } from 'src/app/services/mladen-custom-fields.service';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MladenCustomFields } from 'src/app/interfaces/mladen-custom-fields';

@Component({
  selector: 'app-create-profile-guest',
  templateUrl: './create-profile-guest.component.html',
  styleUrls: ['./create-profile-guest.component.css']
})
export class CreateProfileGuestComponent implements OnInit {

  private formGroup : FormGroup;
  private dataSource : [];
  private subscription : Subscription = new Subscription();

  private polja : MladenCustomFields[];

  constructor(
    private service : MladenCustomFieldsService,
    private fb : FormBuilder
  ) { }

  ngOnInit() {

    this.formGroup = this.fb.group({
        items : this.fb.array([])
    })

    this.subscription.add(this.service.getAll().subscribe(data => {
      this.dataSource = <[]>data;
      
      this.polja = data;

      this.polja.forEach(polje => {
        let validatori = [];
        if(polje.required){
          validatori.push(Validators.required);
        }
        (<FormArray>this.formGroup.get('items')).push(new FormControl('', validatori));
      });      

    }));

  }

  public save(){
    console.log(this.formGroup.value);
  }



}
