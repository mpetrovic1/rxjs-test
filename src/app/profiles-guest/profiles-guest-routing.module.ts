import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProfileGuestComponent } from './create-profile-guest/create-profile-guest.component';

const routes: Routes = [
  {
    path: '',
    component: CreateProfileGuestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesGuestRoutingModule { }
