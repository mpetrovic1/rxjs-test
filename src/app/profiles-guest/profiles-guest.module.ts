import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilesGuestRoutingModule } from './profiles-guest-routing.module';
import { CreateProfileGuestComponent } from './create-profile-guest/create-profile-guest.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatInputModule, MatFormFieldModule, MatIconModule, MatSelectModule, MatTableModule, MatSortModule, MatListModule, MatPaginatorModule, MatSlideToggleModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';

@NgModule({
  declarations: [CreateProfileGuestComponent],
  imports: [
    CommonModule,
    ProfilesGuestRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatListModule,
    FormsModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class ProfilesGuestModule { }
