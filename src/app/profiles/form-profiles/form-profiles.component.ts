import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MladenCustomFields } from 'src/app/interfaces/mladen-custom-fields';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormMladenCustomFieldsComponent } from 'src/app/mladen-custom-fields/components/form-mladen-custom-fields/form-mladen-custom-fields.component';

@Component({
  selector: 'app-form-profiles',
  templateUrl: './form-profiles.component.html',
  styles: []
})
export class FormProfilesComponent implements OnInit {

  private formGroup : FormGroup;
  private dataSource : [];
  private subscription : Subscription = new Subscription();

  private profile;
  private originalObj;

  private polja : MladenCustomFields[];

  constructor(
    private fb : FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef : MatDialogRef<FormMladenCustomFieldsComponent>,
  ) { }

  ngOnInit() {
    
    this.formGroup = this.fb.group({
      
    })

    this.profile = this.data['profile'];
    this.originalObj = this.data['profile'];

    this.dataSource = <[]>this.data['polja'];
    
    this.polja = this.data['polja'];

    this.polja.forEach(polje => {
      let validatori = [];
      let field = 'field_' + polje.id;

      if(polje.type === 'SELECT')
        console.log(this.profile[field]);

      if(polje.required){
        validatori.push(Validators.required);
      }

      let value = this.profile[field];

      if(polje.type === 'DATE') {
        let date = this.profile[field].split('/');
        let day = date[1];
        let month = date[0] - 1;
        let year = date[2];
        value = new Date(year, month, day);
      }

      this.formGroup.addControl(field, new FormControl(value, validatori));

    });

  }

  public save(){

    let editedObj = this.formGroup.value;
    let originalObj = JSON.parse(JSON.stringify(this.originalObj));
    let copy = [];

    delete originalObj['id'];

    for(let i of Object.keys(originalObj)){
        
      if(typeof(originalObj[i]) === 'object'){
        if(JSON.stringify(originalObj[i]) !== JSON.stringify(editedObj[i])){
          copy[i] = editedObj[i];
        }
      }
      else {

        let id = i.split('_')[1];
        if(this.data.polja.filter(field => field.id == id)[0].type === 'DATE'){

          let date = originalObj[i].split('/');
          let day = date[1];
          let month = date[0] - 1;
          let year = date[2];
          originalObj[i] = (new Date(year, month, day)).toString();

        }
        
        if(originalObj[i] != editedObj[i]){          
          copy[i] = editedObj[i];
        }  
      }

    }

    copy['id'] = this.profile.id;

    console.log(copy);

//    this.data['profile'] = editedObj;

  }
  
  public close(){
    this.dialogRef.close();
  }

}
