import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableProfilesComponent } from './table-profiles/table-profiles.component';

const routes: Routes = [
  {
    path: '',
    component: TableProfilesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
