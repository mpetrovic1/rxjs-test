import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilesRoutingModule } from './profiles-routing.module';
import { FormProfilesComponent } from './form-profiles/form-profiles.component';
import { TableProfilesComponent } from './table-profiles/table-profiles.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [FormProfilesComponent, TableProfilesComponent],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    SharedModule
  ],
  entryComponents: [FormProfilesComponent]
})
export class ProfilesModule { }
