import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Subscription } from 'rxjs';
import { MladenCustomFieldsService } from 'src/app/services/mladen-custom-fields.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormProfilesComponent } from '../form-profiles/form-profiles.component';

@Component({
  selector: 'app-table-profiles',
  templateUrl: './table-profiles.component.html',
  styles: [],
  animations: [
    trigger('showColumnsList', [
      state('true', style({
        // transition?
        display: 'block'
      })),
      state('false', style({
        display: 'none'
      })),
      transition('* => *', [
        animate('0.4s')
      ])
    ]),
    
  ]
})
export class TableProfilesComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort;

  public visibilityOfColumns = false;
  public dataSource;
  public fields;
  private subscription = new Subscription();

  public pageSizeOptions = [2, 5, 10, 20, 50, 100];
  public defaultPageSize = 5;
  
  public columns = []

  public displayedColumns : string[];

  public matDataSource = new MatTableDataSource();

  constructor(
    private profilesService : ProfilesService,
    private mladenCustomFieldsService : MladenCustomFieldsService,
    private dialog : MatDialog
  ) { }

  ngOnInit() {

    this.subscription.add(this.profilesService.getAll().subscribe(data => {
      this.dataSource = data;
      this.matDataSource.data = data as [];

      this.matDataSource.paginator = this.paginator;
      this.matDataSource.sort = this.sort;

    }));


    this.subscription.add(this.mladenCustomFieldsService.getAll().subscribe(data => {
      
      this.fields = data;

      data.forEach(element => {

        let name = 'field_' + element.id;
        let displayName = element.label;
        let display = true;
        let multiSelect = false;
        let select = false;

        if(element.type === 'MULTISELECT')
          multiSelect = true;
        else if(element.type === 'SELECT')
          select = true;
          

        this.columns.push({
          name : name,
          displayName : displayName,
          display : display,
          multiSelect : multiSelect,
          select : select
        });
      });

      // edit
      this.columns.push({
        name : 'edit',
        displayName : 'edit',
        display : true
      });

      this.displayedColumns = this.columns.filter(column => column.display === true).map(column => column.name);

//      console.log(this.dataSource);
    }));
  }

  public showHideColumns(ddlColumns){
    ddlColumns.forEach(element => {
      console.log(element.value)
    });
  }

  public search(value : string){
    this.matDataSource.filter = value.trim().toLowerCase();
  }
  
  public changeVisibilityOfColumnsList(){
    this.visibilityOfColumns = !this.visibilityOfColumns;
  }

  // public camelCaseToDisplayName(value : string){
  //   let x = value.replace(/([A-Z\d])/, " $1");
  //   x = x.charAt(0).toUpperCase() + x.slice(1);
  //   return x;
  // }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public addRemoveColumn(selected : any){
    let selectedColumns = selected.map(x => x.value);
    let foundColumns = this.columns.filter(x => selectedColumns.indexOf(x.name) != -1);

    this.displayedColumns = foundColumns.map(x => x.name); 
  }

  public getColumnsForDisplay(){
    return this.displayedColumns;
  }

  public openDialog(id = null){
    this.dialog.open(FormProfilesComponent, {
      height: '550px',
      width: 'auto',
      data: {
        profile: this.dataSource.filter(profile => profile.id === id)[0],
        dataSource: this.dataSource,
        polja : this.fields
      }
    });
  }

  public findSelectedValue(columnName, optionValue){
    let id = columnName.split('_')[1];
    
    return this.fields.filter(field => field.id == id)[0]
    .options.filter(option => option.id == optionValue)[0]
    .optionValue;
  }

}
