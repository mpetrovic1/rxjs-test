import { Component, OnInit, Inject } from '@angular/core';
import { RoomsService } from 'src/app/services/rooms.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-form-rooms',
  templateUrl: './form-rooms.component.html',
  styleUrls: ['./form-rooms.component.css']
})
export class FormRoomsComponent implements OnInit {

  public id;

  public formRooms : FormGroup = this.fb.group({
    id: [],
    title: new FormControl('', Validators.required),
    title_multiple: new FormControl('', Validators.required), 
    //prefix : new FormControl('', Validators.required),
    firstIncremental: new FormControl('', Validators.required),
    secondIncremental: new FormControl('', Validators.required),
    thirdIncremental: new FormControl('', Validators.required),
    roomTypeId: new FormControl('', Validators.required),
    floorId: new FormControl('', Validators.required),
    roomClassId: new FormControl('', Validators.required)
  });

  public roomOption : string = 'multipleIncremental';
//  public defaultRoomOption : string = 'single';

  public roomOptions = [
    {
      displayName: 'Single Room',
      value: 'single',
      id: 1
    },
    {
      displayName: 'Multiple Rooms - comma separated ',
      value: 'multipleComma',
      id: 2
    },
    {
      displayName: 'Multiple Rooms - incremental room numbers ',
      value: 'multipleIncremental',
      id: 3
    }
  ];

  public roomTypes;
  public floors;
  public roomClasses;

  public _roomTypes;
  public _floors;
  public _roomClasses;
  
  public _async_load: Observable<any>;
  private _subscription = new Subscription();


  constructor(
    private fb: FormBuilder,
    private roomsService: RoomsService,
    private dialogRef: MatDialogRef<FormRoomsComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.id = this.data.id;
    this._roomTypes = this.roomsService.getRoomTypes();
    this._floors = this.roomsService.getFloors();
    this._roomClasses = this.roomsService.getRoomClasses();

    const _forSubscribe: any[] = [this._roomTypes, this._floors, this._roomClasses];
    
    this._async_load = forkJoin(_forSubscribe);
    this._subscription.add(this._async_load.subscribe(
      data => {
        this.roomTypes = data[0];
        this.floors = data[1]['data'];
        this.roomClasses = data[2]['data'];

        this.fillForm();
      }
    ));

    // this.roomsService.getRoomTypes().subscribe(data => {
    //   this.roomTypes = data;
    // });
    // this.roomsService.getFloors().subscribe(data => {
    //   this.floors = data['data'];
    // });
    // this.roomsService.getRoomClasses().subscribe(data => {
    //   this.roomClasses = data['data'];
    // });
    
  }

  public fillForm(){
    if(this.id != null) {
      this.roomsService.get(this.id).subscribe(data => {
        let obj = data['data'];
        obj = this.fromIdToText(obj);

        this.formRooms = this.fb.group({
          id: new FormControl(this.id),
          title: new FormControl(obj['title'], Validators.required),
          roomTypeId: new FormControl(obj['roomTypeId'], Validators.required),
          floorId: new FormControl(obj['floorId'], Validators.required),
          roomClassId: new FormControl(obj['roomClassId'], Validators.required)
        });

      });
    }
    else {
      this.changeRadioButton(this.roomOption);
    }
  }

  public changeRadioButton(option){

    console.log(option);
    let selectedOption = typeof(option) === 'string' ? option : option.value;
    this.roomOption = selectedOption;

    switch(selectedOption){

      case 'single':
          this.formRooms = this.fb.group({
            id: [],
            title: new FormControl('', Validators.required),
            roomTypeId: new FormControl('', Validators.required),
            floorId: new FormControl('', Validators.required),
            roomClassId: new FormControl('', Validators.required)
          });
      break;
      case 'multipleComma':
          this.formRooms = this.fb.group({
            id: [],
            title_multiple: new FormControl('', Validators.required),
            roomTypeId: new FormControl('', Validators.required),
            floorId: new FormControl('', Validators.required),
            roomClassId: new FormControl('', Validators.required)
          });
      break;
      case 'multipleIncremental':
          this.formRooms = this.fb.group({
            id: [],
            firstIncremental: new FormControl('', Validators.required),
            secondIncremental: new FormControl('', Validators.required),
            thirdIncremental: new FormControl('', Validators.required),
            roomTypeId: new FormControl('', Validators.required),
            floorId: new FormControl('', Validators.required),
            roomClassId: new FormControl('', Validators.required)
          });
      break;

    }

  }

  public isSaveDisabled(){
    return this.formRooms.invalid;
  }

  public close(){
    this.dialogRef.close();
  }

  public save(){
    let properties;
    
    if(this.id != null) {
      properties = {}
      properties['data'] = this.formRooms.value;
      properties['data'] = this.setData(properties['data']);
      
      //console.log(properties);
      let id = properties['data'].id;
      delete properties['data']['id'];

      console.log(properties);
      this.roomsService.patch(id, properties).subscribe(data => {
        console.log(data);
      });
    }

    else {
      if(this.roomOption === 'multipleIncremental'){
        let from = this.formRooms.get('secondIncremental').value;
        let to = this.formRooms.get('thirdIncremental').value;
        properties = {
          data : []
        }
        let y = this.formRooms.get('firstIncremental').value;
        for(let i = from; i<=to; i++){
          let tmp = this.formRooms.get('firstIncremental').value + i;
          // zbog prenosa po referenci
          let formTmp =  JSON.parse(JSON.stringify(this.formRooms.value));
          formTmp['title'] = tmp;
          formTmp = this.setData(formTmp);
          properties['data'].push(formTmp);
        }
      }
  
      else if(this.roomOption === 'multipleComma') {
        properties = {}
        properties['data'] = [this.formRooms.value];
        properties['data'][0] = this.setData(properties['data'][0]);
        properties['data'][0]['title'] = properties['data'][0]['title_multiple'];
      }
  
      else {
        properties = {}
        properties['data'] = [this.formRooms.value];
        properties['data'][0] = this.setData(properties['data'][0]);
      }
  
      //    console.log(properties);
  
      this.roomsService.post(properties).subscribe(data => {
        console.log(data)
      });
    }

  }

  public changeRoomType(id){
    console.log(id);
  }

  // from text to id
  setData(properties) {
    let roomTypeTitle = properties['roomTypeId'];
    properties['roomTypeId'] = this.roomTypes.filter(type => type.title == roomTypeTitle)[0].id;

    let floorTitle = properties['floorId'];
    properties['floorId'] = this.floors.filter(floor => floor.title == floorTitle)[0].id;

    let roomClassTitle = properties['roomClassId'];
    properties['roomClassId'] = this.roomClasses.filter(roomClass => roomClass.name == roomClassTitle)[0].id;

    return properties;
  }

  private fromIdToText(properties) {
    let roomTypeId = properties['roomTypeId'];
    properties['roomTypeId'] = this.roomTypes.filter(type => type.id == roomTypeId)[0].title;
    
    let floorId = properties['floorId'];
    properties['floorId'] = this.floors.filter(floor => floor.id == floorId)[0].title;

     let roomClassId= properties['roomClassId'];
     properties['roomClassId'] = this.roomClasses.filter(roomClass => roomClass.id == roomClassId)[0].name;

    return properties;
  }

}
