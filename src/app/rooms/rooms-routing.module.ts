import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableRoomsComponent } from './table-rooms/table-rooms.component';

const routes: Routes = [
  {
    path: '',
    component: TableRoomsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomsRoutingModule { }
