import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomsRoutingModule } from './rooms-routing.module';
import { TableRoomsComponent } from './table-rooms/table-rooms.component';
import { FormRoomsComponent } from './form-rooms/form-rooms.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TableRoomsComponent, TableRoomsComponent, FormRoomsComponent],
  imports: [
    CommonModule,
    RoomsRoutingModule,
    SharedModule
  ],
  entryComponents: [FormRoomsComponent]
})
export class RoomsModule { }
