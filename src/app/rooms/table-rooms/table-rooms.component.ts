import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { RoomsService } from 'src/app/services/rooms.service';
import { FormRoomsComponent } from '../form-rooms/form-rooms.component';
import { Room } from 'src/app/interfaces/room';

@Component({
  selector: 'app-table-rooms',
  templateUrl: './table-rooms.component.html',
  styleUrls: ['./table-rooms.component.css'],
  animations: [
    trigger('showColumnsList', [
      state('true', style({
        // transition?
        display: 'block'
      })),
      state('false', style({
        display: 'none'
      })),
      transition('* => *', [
        animate('0.4s')
      ])
    ])
    
  ]
})
export class TableRoomsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort;

  private isVisibleColumnsList : boolean = false;
  public pageSizeOptions = [2, 5, 10, 20, 50, 100];
  public defaultPageSize = 10;

  private subscription = new Subscription();

  public matDataSource = new MatTableDataSource<Room>();
  // kao pomocni niz
  public dataSource = [];

  public checkboxesForDelete = [];
  
  public columns = [
    {
      name: "chb",
      displayName: "Check",
      display: true
    },
    {
      name: "id",
      displayName: "#",
      display: true
    },
    {
      name: "title",
      displayName: "Room Code",
      display: true
    },
    {
      name: "roomStatus",
      displayName: "Room Status",
      display: true
    },
    {
      name: "roomType",
      displayName: "Room Type",
      display: true
    },
    {
      name: "floor",
      displayName: "Floor",
      display: true
    },
    {
      name: "roomClass",
      displayName: "Room Class",
      display: true
    },
    {
      name: "edit",
      displayName: "Edit",
      display: true
    }
  ]

  public displayedColumns : string[] = this.columns.filter(column => column.display === true).map(column => column.name);

  constructor(
    private service : RoomsService,
    private dialog : MatDialog
  ) { }

  ngOnInit() {
    this.getAll();
  }

  public getColumnsForDisplay(){
    return this.displayedColumns;
  }

  public changeVisibilityOfColumns(){
    this.isVisibleColumnsList = !this.isVisibleColumnsList;
  }

  public onSearch(value : string){
    this.matDataSource.filter = value.trim().toLowerCase();
  }

  public getAll(){
    this.subscription.add(this.service.getAll().subscribe(
      response => {
        // probati kasnije bez kastovanja
        this.dataSource = response as Room[];
        this.matDataSource.data = response as Room[];

        this.matDataSource.paginator = this.paginator;
        this.matDataSource.sort = this.sort;
      }
    ))
  }

  public openDialog(id = null){
    //console.log(id);

    this.dialog.open(FormRoomsComponent, {
      height: '500px',
      width: 'auto',
      data: {
        id: id,
        dataSource: this.dataSource
      }
    });
  }

  public addRemoveColumn(selected : any){
    let selectedColumns = selected.map(x => x.value);
    let foundColumns = this.columns.filter(x => selectedColumns.indexOf(x.name) != -1);

    this.displayedColumns = foundColumns.map(x => x.name); 
  }

  public onCheck(item){
    if(item.checked)
      this.checkboxesForDelete.push(<number>item.source.value);
    else
    this.checkboxesForDelete = this.checkboxesForDelete.filter(x => x != <number>item.source.value);

    //console.log(this.checkboxesForDelete);
    console.log(this.checkboxesForDelete);
  }

  public delete(){
      this.service.delete(this.checkboxesForDelete).subscribe(data => {
        console.log(data);
    })
  }

}
