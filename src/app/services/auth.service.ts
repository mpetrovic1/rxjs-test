import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config, apis } from '../config/apis';

import { JwtHelperService } from "@auth0/angular-jwt";
//import { JwtHelperService } from "@auth0/angular-jwt";
//rxjs/internal/observable/from


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  public login(password, username){
    return this.http.post(config + apis.login, {
      client_id: "FDA",
      grant_type: "password",
      password: password,
      scope: "offline_access profile email",
      username: username
    });
  }

  public getHotels(){
    return this.http.get(config + apis.hotels);
  }

  public modify(accountid, hotelname, userid){
    return this.http.post(config + apis.modify, {
      accountid: accountid,
      hotelname: hotelname,
      userid: userid
    });
  }

  public getInformation(){
    let jwtHelper = new JwtHelperService();
    let tokenObject = jwtHelper.decodeToken(localStorage.getItem('token'));

    return tokenObject;
  }

}
