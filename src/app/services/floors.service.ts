import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apis } from '../config/apis';


@Injectable({
  providedIn: 'root'
})
export class FloorsService {

  constructor(private http : HttpClient) { }

  getAll () {
    return this.http.get(apis.floors);
  }

}
