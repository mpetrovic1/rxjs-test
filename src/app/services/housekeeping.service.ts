import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apis, config } from '../config/apis';

@Injectable({
  providedIn: 'root'
})
export class HousekeepingService {

  constructor(
    private http: HttpClient
  ) { }

  getAll () {
    return this.http.get(config + apis.housekeepingDashboard);
  }

  getEmployees(){
    return this.http.get(config + apis.employees);
  }

  getStatuses(){
    return this.http.get(config + apis.housekeepingStatuses);
  }
}
