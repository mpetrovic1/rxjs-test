import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MladenCustomFields } from '../interfaces/mladen-custom-fields';

@Injectable({
  providedIn: 'root'
})
export class MladenCustomFieldsService {

  private url = 'http://localhost:4200/assets/data/mladen-custom-fields.json';

  constructor(
    private http : HttpClient
  ) { }

  public getAll(){
    return this.http.get<MladenCustomFields[]>(this.url);
  }
}
