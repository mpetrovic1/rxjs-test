import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderProduct } from '../interfaces/order-product';

@Injectable({
  providedIn: 'root'
})
export class OrderProductsService {

  private url = 'http://localhost:4200/assets/data/order-products.json';

  constructor(private http : HttpClient) { }

  getAll(){
    return this.http.get<OrderProduct[]>(this.url);
  }
}
