import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {

  private url = 'http://localhost:4200/assets/data/profiles.json';

  constructor(
    private http: HttpClient
  ) { }

  public getAll(){
    return this.http.get(this.url);
  }

}
