import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apis, config } from '../config/apis';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(
    private http: HttpClient
  ) { }

  getAll () {
    return this.http.get(config + apis.rooms);
  }

  getRoomTypes() {
    return this.http.get(config + apis.roomTypes);
  }

  getFloors() {
    return this.http.get(config + apis.floorsApi);
  }

  getRoomClasses() {
    return this.http.get(config + apis.roomClasses);
  }

  public post(obj) {
    return this.http.post(config + apis.rooms, obj);
  }

  public get(id) {
    return this.http.get(config + apis.rooms + '/' + id);
  }

  public patch(id, obj) {
    return this.http.patch(config + apis.rooms + '/' + id, obj);
  }

  public delete(obj) {
    return this.http.request('delete', config + apis.rooms, {body: obj});
  }

}
