import { Injectable, Injector } from "@angular/core";
import { HttpHandler, HttpEvent, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = {
            setHeaders: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }
        let changedRequest = request.clone(headers);
        return next.handle(changedRequest);
    }
}

